using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    [SerializeField]
    private Ghost[] _ghosts;
    [SerializeField]
    private Player _player;
    [SerializeField]
    private Transform _pellets;

    private int _ghostMultiplier = 1;
    private int _score;
    private int _lives;

    private void Start()
    {
        NewGame();
    }
    
    private void NewGame()
    {
        SetScore(0);
        SetLives(3);
        NewRound();
    }

    private void NewRound()
    {
        foreach (Transform pellet in _pellets)
        {
            pellet.gameObject.SetActive(true);
        }

        ResetState();
    }

    private void ResetState()
    {
        ResetGhostMultiplayer();
        SetState(true);
    }

    private void ResetGhostMultiplayer()
    {
        _ghostMultiplier = 1;
    }

    private void GameOver()
    {
        SetState(false);
    }

    private void SetState(bool state)
    {
        for (int i = 0; i < _ghosts.Length; i++)
        {
            _ghosts[i].gameObject.SetActive(state);
        }

        _player.gameObject.SetActive(state);
    }

    private void SetLives(int lives)
    {
        _lives = lives;
    }

    private void AddScore(int score)
    {
        _score += score;
    }

    private void SetScore(int score)
    {
        _score = score;
    }

    private void GhostEaten(Ghost ghost)
    {
        int points = ghost.Points * _ghostMultiplier;
        AddScore(points);
        _ghostMultiplier++;
    }

    private void PlayerEaten()
    {
        _player.gameObject.SetActive(false);
        SetLives(_lives--);

        if (_lives > 0)
        {
            ResetState();
        }
        else
        {
            GameOver();
        }
    }

    public void PelletEaten(Pellet pellet)
    {
        pellet.gameObject.SetActive(false);
        AddScore(pellet.Points);

        if (!HasRemainingPellets())
        {
            Invoke(nameof(NewRound), 3.0f);
        }
    }

    public void PowerPelletEaten(PowerPellet pellet)
    {
        PelletEaten(pellet);
        CancelInvoke();
        Invoke(nameof(ResetGhostMultiplayer), pellet.Duration);
    }

    private bool HasRemainingPellets()
    {
        foreach(Transform pellet in _pellets)
        {
            if (pellet.gameObject.activeSelf)
            {
                return true;
            }
        }

        return false;
    }
       
}
