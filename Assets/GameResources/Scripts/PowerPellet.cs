using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPellet : Pellet
{
    [SerializeField]
    private float _duration = 8f;

    public float Duration => _duration;

    protected override void Eat()
    {
        FindObjectOfType<Gamemanager>().PowerPelletEaten(this);
    }
}
