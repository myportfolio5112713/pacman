using UnityEngine;

public class Pellet : MonoBehaviour
{
    [SerializeField]
    private int _points = 10;

    public int Points => _points;

    protected virtual void Eat()
    {
        FindObjectOfType<Gamemanager>().PelletEaten(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            Eat();
        }
    }
}
