using UnityEngine;

[RequireComponent(typeof(CharacterAnimator), typeof(Rigidbody2D))]
public class Movement : MonoBehaviour
{        
    [SerializeField]
    private float _speed = 8f;
    [SerializeField]
    private float _speedMultiplier = 1f;
    [SerializeField]
    private Vector2 _initialDirection;
    [SerializeField]
    private LayerMask _obstacleLayer;

    private Rigidbody2D _rigidbody;
    private Vector2 _direction;
    private Vector2 _nextDirection;    
    private Vector3 _staringPosition;
    private CharacterAnimator _characterAnimator;

    public Vector2 Direction => _direction;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _characterAnimator = GetComponent<CharacterAnimator>();
        _staringPosition = transform.position;
    }

    private void Start()
    {
        ResetState();
        _characterAnimator.AnimationMoving();
    }

    private void ResetState()
    {
        _speedMultiplier = 1f;
        _direction = _initialDirection;
        _nextDirection = Vector2.zero;
        transform.position = _staringPosition;
        _rigidbody.isKinematic = false;
        this.enabled = true;
    }

    private void Update()
    {
        if(_nextDirection != Vector2.zero)
        {
            SetDirection(_nextDirection);
        }
    }

    private void FixedUpdate()
    {
        Vector2 position = _rigidbody.position;
        Vector2 translation = _direction * _speed * _speedMultiplier * Time.fixedDeltaTime;
        _rigidbody.MovePosition(position + translation);
    }

    public void SetDirection(Vector2 direction, bool forced = false)
    {
        if (forced || !Occupied(direction))
        {
            _direction = direction;
            _nextDirection = Vector2.zero;
        }
        else
        {
            _nextDirection = direction;
        }
    }

    private bool Occupied(Vector2 direction)
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, Vector2.one * 0.75f, 0f, direction, 1.5f, _obstacleLayer);
        return hit.collider != null;
    }
}
