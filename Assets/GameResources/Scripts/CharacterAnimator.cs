using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    private void Awake()
    {       
        _animator = GetComponent<Animator>();
    }

    public void AnimationIdle()
    {
        _animator.SetTrigger("Idle");
    }

    public void AnimationMoving()
    {
        _animator.SetTrigger("Move");
    }

    public void AnimationDeath()
    {
        _animator.SetTrigger("Death");
    }  

    public void CanEated()
    {
        _animator.SetTrigger("CanEated");
    }
}
